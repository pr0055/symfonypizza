<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_e46a0f66c58704cb53b1cdc1c754048ee0b10fcc55be3481c7e0e62235d4564b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a2a6367c5eeadf9c8d7297ffff8372f3893dad0bd39a238e9a2dc181cc58de47 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a2a6367c5eeadf9c8d7297ffff8372f3893dad0bd39a238e9a2dc181cc58de47->enter($__internal_a2a6367c5eeadf9c8d7297ffff8372f3893dad0bd39a238e9a2dc181cc58de47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_a2a6367c5eeadf9c8d7297ffff8372f3893dad0bd39a238e9a2dc181cc58de47->leave($__internal_a2a6367c5eeadf9c8d7297ffff8372f3893dad0bd39a238e9a2dc181cc58de47_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
    }
}
