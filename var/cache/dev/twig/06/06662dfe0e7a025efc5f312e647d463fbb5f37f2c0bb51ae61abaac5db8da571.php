<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_bdc69628f4574abb1283b9440eee23a79e9e115b34de58cea17a7729a7bfb64b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0987c5f67b545291276ce8e5f29395108294fe4e2ebc862d97ec1e565c00127f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0987c5f67b545291276ce8e5f29395108294fe4e2ebc862d97ec1e565c00127f->enter($__internal_0987c5f67b545291276ce8e5f29395108294fe4e2ebc862d97ec1e565c00127f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_0987c5f67b545291276ce8e5f29395108294fe4e2ebc862d97ec1e565c00127f->leave($__internal_0987c5f67b545291276ce8e5f29395108294fe4e2ebc862d97ec1e565c00127f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
    }
}
