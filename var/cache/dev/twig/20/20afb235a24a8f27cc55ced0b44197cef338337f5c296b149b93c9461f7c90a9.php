<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_bea9055a9407e8b7a6f5c9d849240a87aea8074dc4299591d7ad5f9fedeac6a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9b510e042708a2630a216870e76259ce4885260fcd3f73548e731c62d961a993 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9b510e042708a2630a216870e76259ce4885260fcd3f73548e731c62d961a993->enter($__internal_9b510e042708a2630a216870e76259ce4885260fcd3f73548e731c62d961a993_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_9b510e042708a2630a216870e76259ce4885260fcd3f73548e731c62d961a993->leave($__internal_9b510e042708a2630a216870e76259ce4885260fcd3f73548e731c62d961a993_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?>
";
    }
}
