<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_9d15db7090d18bcb7e707308062c5b6d79468a989d7e913871be9d9b7d91a316 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1114e6e58667a9e2de997c13d371d29446f9f8f8ec299230c2d19247abcd1648 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1114e6e58667a9e2de997c13d371d29446f9f8f8ec299230c2d19247abcd1648->enter($__internal_1114e6e58667a9e2de997c13d371d29446f9f8f8ec299230c2d19247abcd1648_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_1114e6e58667a9e2de997c13d371d29446f9f8f8ec299230c2d19247abcd1648->leave($__internal_1114e6e58667a9e2de997c13d371d29446f9f8f8ec299230c2d19247abcd1648_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
    }
}
