<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_1d572224fdbaa1f9bffee44df85c2f02583347f24b96c4cce8c2fa7dd4a29955 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_025d5d098fbbf76282f374a1fc36639b94117d05f9f3481b7c007ec0ffecaf0a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_025d5d098fbbf76282f374a1fc36639b94117d05f9f3481b7c007ec0ffecaf0a->enter($__internal_025d5d098fbbf76282f374a1fc36639b94117d05f9f3481b7c007ec0ffecaf0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_025d5d098fbbf76282f374a1fc36639b94117d05f9f3481b7c007ec0ffecaf0a->leave($__internal_025d5d098fbbf76282f374a1fc36639b94117d05f9f3481b7c007ec0ffecaf0a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
    }
}
