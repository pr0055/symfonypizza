<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_62bef7ab8a04f7416a45d24385e6847a7d811fcbb30ded4f505641c580fcd4c9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_73e0e82458f51516f17d039fab4de1a5a3206e34519884bffc98f2f7ef45e5fe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_73e0e82458f51516f17d039fab4de1a5a3206e34519884bffc98f2f7ef45e5fe->enter($__internal_73e0e82458f51516f17d039fab4de1a5a3206e34519884bffc98f2f7ef45e5fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_73e0e82458f51516f17d039fab4de1a5a3206e34519884bffc98f2f7ef45e5fe->leave($__internal_73e0e82458f51516f17d039fab4de1a5a3206e34519884bffc98f2f7ef45e5fe_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
    }
}
