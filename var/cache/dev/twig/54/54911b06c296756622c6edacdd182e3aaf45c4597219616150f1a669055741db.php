<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_d98724319cc249cb6b16d4c0a559341ee7e2472501713a41920f60a95e506503 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7e642d3d4d70161ff53a699b9a4201cb021715d7006d196833274de9cd99b426 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e642d3d4d70161ff53a699b9a4201cb021715d7006d196833274de9cd99b426->enter($__internal_7e642d3d4d70161ff53a699b9a4201cb021715d7006d196833274de9cd99b426_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7e642d3d4d70161ff53a699b9a4201cb021715d7006d196833274de9cd99b426->leave($__internal_7e642d3d4d70161ff53a699b9a4201cb021715d7006d196833274de9cd99b426_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_90fdd14f61d5a005dc73cd95b44c8bedd1fdb0de819aa9548a7ea9e542274a6c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_90fdd14f61d5a005dc73cd95b44c8bedd1fdb0de819aa9548a7ea9e542274a6c->enter($__internal_90fdd14f61d5a005dc73cd95b44c8bedd1fdb0de819aa9548a7ea9e542274a6c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_90fdd14f61d5a005dc73cd95b44c8bedd1fdb0de819aa9548a7ea9e542274a6c->leave($__internal_90fdd14f61d5a005dc73cd95b44c8bedd1fdb0de819aa9548a7ea9e542274a6c_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_f38d62641a18aab8c192c3ac0ab5f114a6f4d76beaceb837fa7d13948bdf6563 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f38d62641a18aab8c192c3ac0ab5f114a6f4d76beaceb837fa7d13948bdf6563->enter($__internal_f38d62641a18aab8c192c3ac0ab5f114a6f4d76beaceb837fa7d13948bdf6563_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_f38d62641a18aab8c192c3ac0ab5f114a6f4d76beaceb837fa7d13948bdf6563->leave($__internal_f38d62641a18aab8c192c3ac0ab5f114a6f4d76beaceb837fa7d13948bdf6563_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
";
    }
}
