<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_69f8501318af1021ffb9e68ac9ded3200b38324c244fd5579d251220f8f28920 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dd1f5c067f5165094d1ba5f5aae46283dfa7c2de6d874624698dd1742fd61252 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dd1f5c067f5165094d1ba5f5aae46283dfa7c2de6d874624698dd1742fd61252->enter($__internal_dd1f5c067f5165094d1ba5f5aae46283dfa7c2de6d874624698dd1742fd61252_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_dd1f5c067f5165094d1ba5f5aae46283dfa7c2de6d874624698dd1742fd61252->leave($__internal_dd1f5c067f5165094d1ba5f5aae46283dfa7c2de6d874624698dd1742fd61252_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
    }
}
