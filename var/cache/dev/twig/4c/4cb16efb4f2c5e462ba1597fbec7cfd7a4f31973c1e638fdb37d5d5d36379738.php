<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_fd540ddef9add1d7f79ea6808b1de4e821ce47d1e47882e4da8b05f77f0be4af extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d6d5f0f8ac73a3f5992d3d4b2c106244bb0e5a91c52b74226ac76faef93362ed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d6d5f0f8ac73a3f5992d3d4b2c106244bb0e5a91c52b74226ac76faef93362ed->enter($__internal_d6d5f0f8ac73a3f5992d3d4b2c106244bb0e5a91c52b74226ac76faef93362ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d6d5f0f8ac73a3f5992d3d4b2c106244bb0e5a91c52b74226ac76faef93362ed->leave($__internal_d6d5f0f8ac73a3f5992d3d4b2c106244bb0e5a91c52b74226ac76faef93362ed_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_06b55d500b8e3326550d7b4797982679d06943fb66d032fab1ae77801246e2f3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_06b55d500b8e3326550d7b4797982679d06943fb66d032fab1ae77801246e2f3->enter($__internal_06b55d500b8e3326550d7b4797982679d06943fb66d032fab1ae77801246e2f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpFoundationExtension')->generateAbsoluteUrl($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_06b55d500b8e3326550d7b4797982679d06943fb66d032fab1ae77801246e2f3->leave($__internal_06b55d500b8e3326550d7b4797982679d06943fb66d032fab1ae77801246e2f3_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_38554303cc1e9b374ae63bf6da0babe4cc88b4869dbc2e92871c13d7598739eb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_38554303cc1e9b374ae63bf6da0babe4cc88b4869dbc2e92871c13d7598739eb->enter($__internal_38554303cc1e9b374ae63bf6da0babe4cc88b4869dbc2e92871c13d7598739eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_38554303cc1e9b374ae63bf6da0babe4cc88b4869dbc2e92871c13d7598739eb->leave($__internal_38554303cc1e9b374ae63bf6da0babe4cc88b4869dbc2e92871c13d7598739eb_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_63421111483b664379c11e21c9a2a2cab97724c48ab3a8c01a3f9ae1c5c4b9d4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_63421111483b664379c11e21c9a2a2cab97724c48ab3a8c01a3f9ae1c5c4b9d4->enter($__internal_63421111483b664379c11e21c9a2a2cab97724c48ab3a8c01a3f9ae1c5c4b9d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_63421111483b664379c11e21c9a2a2cab97724c48ab3a8c01a3f9ae1c5c4b9d4->leave($__internal_63421111483b664379c11e21c9a2a2cab97724c48ab3a8c01a3f9ae1c5c4b9d4_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <link href=\"{{ absolute_url(asset('bundles/framework/css/exception.css')) }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
";
    }
}
