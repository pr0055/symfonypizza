<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_26712c2ba137455b750246690fa6bfeb26421a7bb8b57aa9b062218611ac114f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b9a147a076b78e4fc0823da7a11900b8b7b8559dd0c10655366bb0d2522e55fa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b9a147a076b78e4fc0823da7a11900b8b7b8559dd0c10655366bb0d2522e55fa->enter($__internal_b9a147a076b78e4fc0823da7a11900b8b7b8559dd0c10655366bb0d2522e55fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_b9a147a076b78e4fc0823da7a11900b8b7b8559dd0c10655366bb0d2522e55fa->leave($__internal_b9a147a076b78e4fc0823da7a11900b8b7b8559dd0c10655366bb0d2522e55fa_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
    }
}
