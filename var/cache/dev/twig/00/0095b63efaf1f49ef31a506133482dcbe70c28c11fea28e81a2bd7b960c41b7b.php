<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_e14dd821aee709d8457c7e3a5ce93d6afa3f4de345981799d6f7fe5fb8973803 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_663b6a82eb5689041626c8245e5260a7cfebf0965800b1578dac7105910a93f3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_663b6a82eb5689041626c8245e5260a7cfebf0965800b1578dac7105910a93f3->enter($__internal_663b6a82eb5689041626c8245e5260a7cfebf0965800b1578dac7105910a93f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_663b6a82eb5689041626c8245e5260a7cfebf0965800b1578dac7105910a93f3->leave($__internal_663b6a82eb5689041626c8245e5260a7cfebf0965800b1578dac7105910a93f3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
    }
}
