<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_94f226a3e4bc3e8927eb34dcd2b8152f739b0c1d3375aeaa7687cd4288298600 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ecaac6c9058305949b34a5887c47ded395f7715294284faf799fee97227890e4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ecaac6c9058305949b34a5887c47ded395f7715294284faf799fee97227890e4->enter($__internal_ecaac6c9058305949b34a5887c47ded395f7715294284faf799fee97227890e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_ecaac6c9058305949b34a5887c47ded395f7715294284faf799fee97227890e4->leave($__internal_ecaac6c9058305949b34a5887c47ded395f7715294284faf799fee97227890e4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
    }
}
