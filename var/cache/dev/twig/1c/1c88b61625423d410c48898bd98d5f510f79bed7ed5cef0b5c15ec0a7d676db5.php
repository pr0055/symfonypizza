<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_5d72eedbafd0f7569a546f039ee26aaf825d17fa83f73b326896a7aa5299e4a5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e5c24c5516a60363ed54aa5a8d513962b27b700ca29cc9237e9e2ef8182576a9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e5c24c5516a60363ed54aa5a8d513962b27b700ca29cc9237e9e2ef8182576a9->enter($__internal_e5c24c5516a60363ed54aa5a8d513962b27b700ca29cc9237e9e2ef8182576a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_e5c24c5516a60363ed54aa5a8d513962b27b700ca29cc9237e9e2ef8182576a9->leave($__internal_e5c24c5516a60363ed54aa5a8d513962b27b700ca29cc9237e9e2ef8182576a9_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }

    public function getSource()
    {
        return "/*
{% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}
*/
";
    }
}
