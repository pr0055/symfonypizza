<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_4fefb4c632e965c7ec3fa29f05b61157e15c479783420d0d888f7f7ede81c3a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_98fd93a9819de5f5b6074903e22acde10319cb7c489ac6843c151ae7f30ae1a4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_98fd93a9819de5f5b6074903e22acde10319cb7c489ac6843c151ae7f30ae1a4->enter($__internal_98fd93a9819de5f5b6074903e22acde10319cb7c489ac6843c151ae7f30ae1a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_98fd93a9819de5f5b6074903e22acde10319cb7c489ac6843c151ae7f30ae1a4->leave($__internal_98fd93a9819de5f5b6074903e22acde10319cb7c489ac6843c151ae7f30ae1a4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
    }
}
