<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_30762608527b10bbbcb88d63be88abf22fc7f65d98de62afa968ee1c0f192d8f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a1348acd91e43b6d0d2a5eaa49064c10b6ab68a278f62c6cc568ea9b01617259 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a1348acd91e43b6d0d2a5eaa49064c10b6ab68a278f62c6cc568ea9b01617259->enter($__internal_a1348acd91e43b6d0d2a5eaa49064c10b6ab68a278f62c6cc568ea9b01617259_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_a1348acd91e43b6d0d2a5eaa49064c10b6ab68a278f62c6cc568ea9b01617259->leave($__internal_a1348acd91e43b6d0d2a5eaa49064c10b6ab68a278f62c6cc568ea9b01617259_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
    }
}
