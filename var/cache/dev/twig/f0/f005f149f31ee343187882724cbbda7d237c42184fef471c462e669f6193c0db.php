<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_aafbc2173020925a5e39a3f6c023b4395f9daa4b869dd065e944abebb8c5fbb2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_77310b1fcf440c516d205918436f3e781fbf31cd1d2845d3a81a1d4fb5f65c3c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_77310b1fcf440c516d205918436f3e781fbf31cd1d2845d3a81a1d4fb5f65c3c->enter($__internal_77310b1fcf440c516d205918436f3e781fbf31cd1d2845d3a81a1d4fb5f65c3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_77310b1fcf440c516d205918436f3e781fbf31cd1d2845d3a81a1d4fb5f65c3c->leave($__internal_77310b1fcf440c516d205918436f3e781fbf31cd1d2845d3a81a1d4fb5f65c3c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'search')) ?>
";
    }
}
