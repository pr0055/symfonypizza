<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_2ae139f6825d3bf92448028352eb44e2b37aad3376a2a19e52f04c99e5c2f048 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4ad59a7b99f5c749e59343899d333a68e08ac20a032e546b2d13cdaf0bbdfd09 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4ad59a7b99f5c749e59343899d333a68e08ac20a032e546b2d13cdaf0bbdfd09->enter($__internal_4ad59a7b99f5c749e59343899d333a68e08ac20a032e546b2d13cdaf0bbdfd09_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_4ad59a7b99f5c749e59343899d333a68e08ac20a032e546b2d13cdaf0bbdfd09->leave($__internal_4ad59a7b99f5c749e59343899d333a68e08ac20a032e546b2d13cdaf0bbdfd09_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
    }
}
