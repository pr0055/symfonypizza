<?php

/* TwigBundle:Exception:error.rdf.twig */
class __TwigTemplate_83f2e9376b925d77cb8535aa97e3e98afa7c07d40dc8234b2066b32ec4c3e8e5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c6791f55bd774605d679b528163e2ef3e487e2ef494350a66d4ace0e28c6c8ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c6791f55bd774605d679b528163e2ef3e487e2ef494350a66d4ace0e28c6c8ac->enter($__internal_c6791f55bd774605d679b528163e2ef3e487e2ef494350a66d4ace0e28c6c8ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "TwigBundle:Exception:error.rdf.twig", 1)->display($context);
        
        $__internal_c6791f55bd774605d679b528163e2ef3e487e2ef494350a66d4ace0e28c6c8ac->leave($__internal_c6791f55bd774605d679b528163e2ef3e487e2ef494350a66d4ace0e28c6c8ac_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.rdf.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "{% include '@Twig/Exception/error.xml.twig' %}
";
    }
}
