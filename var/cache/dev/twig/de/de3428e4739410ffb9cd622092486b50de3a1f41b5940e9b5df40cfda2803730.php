<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_8cb1a1c348b7caaa4294348e871468eac7e6da3c58e1eef25f2ee50d9685e5d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_588faa325cdd1d97d3830255bb5bb2800c2124b92592e2a8d57b09d6b197707a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_588faa325cdd1d97d3830255bb5bb2800c2124b92592e2a8d57b09d6b197707a->enter($__internal_588faa325cdd1d97d3830255bb5bb2800c2124b92592e2a8d57b09d6b197707a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_588faa325cdd1d97d3830255bb5bb2800c2124b92592e2a8d57b09d6b197707a->leave($__internal_588faa325cdd1d97d3830255bb5bb2800c2124b92592e2a8d57b09d6b197707a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'url')) ?>
";
    }
}
