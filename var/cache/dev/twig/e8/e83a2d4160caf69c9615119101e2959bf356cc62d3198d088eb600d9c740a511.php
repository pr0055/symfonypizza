<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_4f8b2dddf22b0e36416b5723bcca1e22acfcf2ab20c918c56cfb975c58b27cd7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_49076a72b7027dc8f528b795f18d850db6631018a1b09e9fedceac666b368adc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_49076a72b7027dc8f528b795f18d850db6631018a1b09e9fedceac666b368adc->enter($__internal_49076a72b7027dc8f528b795f18d850db6631018a1b09e9fedceac666b368adc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, (isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_49076a72b7027dc8f528b795f18d850db6631018a1b09e9fedceac666b368adc->leave($__internal_49076a72b7027dc8f528b795f18d850db6631018a1b09e9fedceac666b368adc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php echo str_replace('{{ widget }}', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
    }
}
