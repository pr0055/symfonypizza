<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_d9c6f55abc59191346699fbf5aa8b1a985835b1265724b2edbae1e6f02d5634a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a367f86bf92a44105399d1dbebf96e6b412ca96257645fbbbf54d77be76602b4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a367f86bf92a44105399d1dbebf96e6b412ca96257645fbbbf54d77be76602b4->enter($__internal_a367f86bf92a44105399d1dbebf96e6b412ca96257645fbbbf54d77be76602b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_a367f86bf92a44105399d1dbebf96e6b412ca96257645fbbbf54d77be76602b4->leave($__internal_a367f86bf92a44105399d1dbebf96e6b412ca96257645fbbbf54d77be76602b4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
    }
}
