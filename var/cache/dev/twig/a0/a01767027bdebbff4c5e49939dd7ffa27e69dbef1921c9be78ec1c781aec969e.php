<?php

/* @Framework/Form/range_widget.html.php */
class __TwigTemplate_e19576c95421fedf91a920be5bdf1124c6bf5fa8d35074612712454006fe5704 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d5b1d200a3907ae4bcc13e9ed5df4f813223f87bcc618a941efc379977fe0255 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d5b1d200a3907ae4bcc13e9ed5df4f813223f87bcc618a941efc379977fe0255->enter($__internal_d5b1d200a3907ae4bcc13e9ed5df4f813223f87bcc618a941efc379977fe0255_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/range_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'range'));
";
        
        $__internal_d5b1d200a3907ae4bcc13e9ed5df4f813223f87bcc618a941efc379977fe0255->leave($__internal_d5b1d200a3907ae4bcc13e9ed5df4f813223f87bcc618a941efc379977fe0255_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/range_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'range'));
";
    }
}
