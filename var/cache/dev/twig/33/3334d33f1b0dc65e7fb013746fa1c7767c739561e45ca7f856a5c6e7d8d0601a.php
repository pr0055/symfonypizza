<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_e8645b13ac7ec958762d2abcc0d514828bf00b700ef18e537a9e4bd85a10d681 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9f806c848aaba263713a778c9f3e9a1f50a72813b579cb38d9f20927d2e37aca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f806c848aaba263713a778c9f3e9a1f50a72813b579cb38d9f20927d2e37aca->enter($__internal_9f806c848aaba263713a778c9f3e9a1f50a72813b579cb38d9f20927d2e37aca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_9f806c848aaba263713a778c9f3e9a1f50a72813b579cb38d9f20927d2e37aca->leave($__internal_9f806c848aaba263713a778c9f3e9a1f50a72813b579cb38d9f20927d2e37aca_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
    }
}
