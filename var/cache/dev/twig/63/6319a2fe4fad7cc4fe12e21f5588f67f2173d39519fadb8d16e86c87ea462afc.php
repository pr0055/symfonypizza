<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_f1b83934251e3080c12a3a28b68c24f925e399b21fca5d642b88d2fe22960f9b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5fcaf162a083de433b8b2eac53819e4e438c0d50b11b0af62cb46c880fd64328 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5fcaf162a083de433b8b2eac53819e4e438c0d50b11b0af62cb46c880fd64328->enter($__internal_5fcaf162a083de433b8b2eac53819e4e438c0d50b11b0af62cb46c880fd64328_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_5fcaf162a083de433b8b2eac53819e4e438c0d50b11b0af62cb46c880fd64328->leave($__internal_5fcaf162a083de433b8b2eac53819e4e438c0d50b11b0af62cb46c880fd64328_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
    }
}
