<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_7b538b0a5b119a5472a3c02041da50e02cb240aa760be1b18a0e1df3bb07e0e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d8a059d9c485aa5463c9c3e09e7fc900470538be6312f86b22509177093238eb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d8a059d9c485aa5463c9c3e09e7fc900470538be6312f86b22509177093238eb->enter($__internal_d8a059d9c485aa5463c9c3e09e7fc900470538be6312f86b22509177093238eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_d8a059d9c485aa5463c9c3e09e7fc900470538be6312f86b22509177093238eb->leave($__internal_d8a059d9c485aa5463c9c3e09e7fc900470538be6312f86b22509177093238eb_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }

    public function getSource()
    {
        return "/*
{{ status_code }} {{ status_text }}

*/
";
    }
}
