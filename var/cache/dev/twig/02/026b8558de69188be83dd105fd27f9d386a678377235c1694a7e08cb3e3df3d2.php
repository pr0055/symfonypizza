<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_e10413f658f26986c05122c384b8c926f57136eebe9b88d87fd3b5030331eb5a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_de1e385bdd573a8be4d1d62f884b0e13b3a1a5031223ec5f569e4a34ad5c4e60 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_de1e385bdd573a8be4d1d62f884b0e13b3a1a5031223ec5f569e4a34ad5c4e60->enter($__internal_de1e385bdd573a8be4d1d62f884b0e13b3a1a5031223ec5f569e4a34ad5c4e60_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_de1e385bdd573a8be4d1d62f884b0e13b3a1a5031223ec5f569e4a34ad5c4e60->leave($__internal_de1e385bdd573a8be4d1d62f884b0e13b3a1a5031223ec5f569e4a34ad5c4e60_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
    }
}
