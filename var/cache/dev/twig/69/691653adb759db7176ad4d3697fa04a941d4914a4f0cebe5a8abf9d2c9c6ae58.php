<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_9364f8f5259fd07eb343860e812060f69020250671edb80c702e281172ce46ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3984d5e787227eacaf9edb1829fb76337eba5e411fb6e56f01bfc53f6304e0b0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3984d5e787227eacaf9edb1829fb76337eba5e411fb6e56f01bfc53f6304e0b0->enter($__internal_3984d5e787227eacaf9edb1829fb76337eba5e411fb6e56f01bfc53f6304e0b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_3984d5e787227eacaf9edb1829fb76337eba5e411fb6e56f01bfc53f6304e0b0->leave($__internal_3984d5e787227eacaf9edb1829fb76337eba5e411fb6e56f01bfc53f6304e0b0_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "{% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}
";
    }
}
