<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_62aa99cadc7030925e8529561fdc784b277c7b0c13eb1faf5fc90ba5cdc8bc7d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c19fe878469873af10471021243ff0ea582915fcac137b9b7c246cbd11555cc7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c19fe878469873af10471021243ff0ea582915fcac137b9b7c246cbd11555cc7->enter($__internal_c19fe878469873af10471021243ff0ea582915fcac137b9b7c246cbd11555cc7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_c19fe878469873af10471021243ff0ea582915fcac137b9b7c246cbd11555cc7->leave($__internal_c19fe878469873af10471021243ff0ea582915fcac137b9b7c246cbd11555cc7_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "{% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}
";
    }
}
