<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_b7da2b5964a07bd772d0270b068d3b6a47a58d724e1f6d315410d649aa596bac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e06f2f2a9b414a22815ad1f3d5c71c10745721e2ad86e6142f44956d2b8fa86e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e06f2f2a9b414a22815ad1f3d5c71c10745721e2ad86e6142f44956d2b8fa86e->enter($__internal_e06f2f2a9b414a22815ad1f3d5c71c10745721e2ad86e6142f44956d2b8fa86e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "TwigBundle:Exception:error.atom.twig", 1)->display($context);
        
        $__internal_e06f2f2a9b414a22815ad1f3d5c71c10745721e2ad86e6142f44956d2b8fa86e->leave($__internal_e06f2f2a9b414a22815ad1f3d5c71c10745721e2ad86e6142f44956d2b8fa86e_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "{% include '@Twig/Exception/error.xml.twig' %}
";
    }
}
