<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_a3ac5eec300b44fbfe393c8ac27b8d8183ba9c47d80536f409b579aa2bdf98a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2059f71b77f724dbbfc7672bf9c3710c6b405ebcfa90b73c063582d81c073ae2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2059f71b77f724dbbfc7672bf9c3710c6b405ebcfa90b73c063582d81c073ae2->enter($__internal_2059f71b77f724dbbfc7672bf9c3710c6b405ebcfa90b73c063582d81c073ae2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_2059f71b77f724dbbfc7672bf9c3710c6b405ebcfa90b73c063582d81c073ae2->leave($__internal_2059f71b77f724dbbfc7672bf9c3710c6b405ebcfa90b73c063582d81c073ae2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php echo \$view['form']->widget(\$form) ?>
";
    }
}
