<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_5e4f7c8f246bd50e01a9b0cf5a2042adf1e914a401260e11ff97f6c884b8b536 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e960b3709bece5ec4a93ad44d154ae220a2227df934157b49da054c06f5aa9a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e960b3709bece5ec4a93ad44d154ae220a2227df934157b49da054c06f5aa9a8->enter($__internal_e960b3709bece5ec4a93ad44d154ae220a2227df934157b49da054c06f5aa9a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_e960b3709bece5ec4a93ad44d154ae220a2227df934157b49da054c06f5aa9a8->leave($__internal_e960b3709bece5ec4a93ad44d154ae220a2227df934157b49da054c06f5aa9a8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
    }
}
