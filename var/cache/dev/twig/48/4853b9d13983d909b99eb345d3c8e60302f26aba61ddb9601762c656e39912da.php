<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_9ebce9299ebd8d6a6e1f0255799ff26908eacac51064f99274497a179ef7a660 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_41015dbb7f420a30d6cdb2bbd590c74d41edf1c6aebb441453f20764640ca151 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_41015dbb7f420a30d6cdb2bbd590c74d41edf1c6aebb441453f20764640ca151->enter($__internal_41015dbb7f420a30d6cdb2bbd590c74d41edf1c6aebb441453f20764640ca151_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_41015dbb7f420a30d6cdb2bbd590c74d41edf1c6aebb441453f20764640ca151->leave($__internal_41015dbb7f420a30d6cdb2bbd590c74d41edf1c6aebb441453f20764640ca151_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }

    public function getSource()
    {
        return "/*
{{ status_code }} {{ status_text }}

*/
";
    }
}
