<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_d5c4d5fd9d9a5acd9668e5ee3516673ecf8de3df01544574577b099636488f76 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f5426e3ddacae127db1612c86d8bfa22fbf2fb8c8a020142bd0379b24ab216b0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f5426e3ddacae127db1612c86d8bfa22fbf2fb8c8a020142bd0379b24ab216b0->enter($__internal_f5426e3ddacae127db1612c86d8bfa22fbf2fb8c8a020142bd0379b24ab216b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_f5426e3ddacae127db1612c86d8bfa22fbf2fb8c8a020142bd0379b24ab216b0->leave($__internal_f5426e3ddacae127db1612c86d8bfa22fbf2fb8c8a020142bd0379b24ab216b0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
    }
}
