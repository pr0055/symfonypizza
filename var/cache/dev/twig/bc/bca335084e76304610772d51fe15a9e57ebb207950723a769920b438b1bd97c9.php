<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_d2b8255208797abbe517b52cc5ce303b1e9d9317a107ac0df1479b49648a19c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_25ff9d612f0570b84b7b5684c08376164a3ffb8f1e89484aa0b28c44f28fa895 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_25ff9d612f0570b84b7b5684c08376164a3ffb8f1e89484aa0b28c44f28fa895->enter($__internal_25ff9d612f0570b84b7b5684c08376164a3ffb8f1e89484aa0b28c44f28fa895_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_25ff9d612f0570b84b7b5684c08376164a3ffb8f1e89484aa0b28c44f28fa895->leave($__internal_25ff9d612f0570b84b7b5684c08376164a3ffb8f1e89484aa0b28c44f28fa895_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_46cdaee3f6629dee40103ab779f42cb04d3c54974b4174bc04c6bc7fb0f7ccee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_46cdaee3f6629dee40103ab779f42cb04d3c54974b4174bc04c6bc7fb0f7ccee->enter($__internal_46cdaee3f6629dee40103ab779f42cb04d3c54974b4174bc04c6bc7fb0f7ccee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_46cdaee3f6629dee40103ab779f42cb04d3c54974b4174bc04c6bc7fb0f7ccee->leave($__internal_46cdaee3f6629dee40103ab779f42cb04d3c54974b4174bc04c6bc7fb0f7ccee_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSource()
    {
        return "{% block panel '' %}
";
    }
}
