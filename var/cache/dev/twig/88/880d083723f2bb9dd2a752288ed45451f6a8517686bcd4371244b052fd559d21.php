<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_56759550bd68ff51bd03fccc2238c50aa37c59c8358b60c3cfedd43145f7e71a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d281254fb44c5feb571d24302438c76cf7983bf99d25c5d2f8e365d8534500f1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d281254fb44c5feb571d24302438c76cf7983bf99d25c5d2f8e365d8534500f1->enter($__internal_d281254fb44c5feb571d24302438c76cf7983bf99d25c5d2f8e365d8534500f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_d281254fb44c5feb571d24302438c76cf7983bf99d25c5d2f8e365d8534500f1->leave($__internal_d281254fb44c5feb571d24302438c76cf7983bf99d25c5d2f8e365d8534500f1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
    }
}
