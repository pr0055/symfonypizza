<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_60178e1ddb4652847ad74d91fad19d18b827c093909160eb64c9265ac387d71b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5e97946f281e0b894b51c2057941bf80d917cc322af3b29f06c94df68dac75fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5e97946f281e0b894b51c2057941bf80d917cc322af3b29f06c94df68dac75fd->enter($__internal_5e97946f281e0b894b51c2057941bf80d917cc322af3b29f06c94df68dac75fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "exception" => $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_5e97946f281e0b894b51c2057941bf80d917cc322af3b29f06c94df68dac75fd->leave($__internal_5e97946f281e0b894b51c2057941bf80d917cc322af3b29f06c94df68dac75fd_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "{{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}
";
    }
}
