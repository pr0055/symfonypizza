<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_dc2cfad07f378f7bf081bd226d50799f548bf9b44ca27c6f74bf09af679ba023 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_87e0ae23ebbc19b78160d3d2ed19e1ef693b55c7902206d4202a710eeabf29db = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_87e0ae23ebbc19b78160d3d2ed19e1ef693b55c7902206d4202a710eeabf29db->enter($__internal_87e0ae23ebbc19b78160d3d2ed19e1ef693b55c7902206d4202a710eeabf29db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_87e0ae23ebbc19b78160d3d2ed19e1ef693b55c7902206d4202a710eeabf29db->leave($__internal_87e0ae23ebbc19b78160d3d2ed19e1ef693b55c7902206d4202a710eeabf29db_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
    }
}
