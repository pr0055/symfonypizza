<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_05cd6baf9d145d24a7976f9d80b19de8a6f7a59e12b1465739eedf89e917802e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3324f11112f227ad57a5963465aaff764387441d8da2a31b3aa6e1c4da09b183 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3324f11112f227ad57a5963465aaff764387441d8da2a31b3aa6e1c4da09b183->enter($__internal_3324f11112f227ad57a5963465aaff764387441d8da2a31b3aa6e1c4da09b183_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_3324f11112f227ad57a5963465aaff764387441d8da2a31b3aa6e1c4da09b183->leave($__internal_3324f11112f227ad57a5963465aaff764387441d8da2a31b3aa6e1c4da09b183_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
    }
}
