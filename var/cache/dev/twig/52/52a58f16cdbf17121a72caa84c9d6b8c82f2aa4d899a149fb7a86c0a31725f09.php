<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_920519b05d8a9a1fbcb70d213bf83f565bba53f975f145d740f8c3e37c0410d9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_57224cd0c199ce4235aa94c0d7cfe69a5a3981ca34aa8e0e105f0fb8be6a496b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_57224cd0c199ce4235aa94c0d7cfe69a5a3981ca34aa8e0e105f0fb8be6a496b->enter($__internal_57224cd0c199ce4235aa94c0d7cfe69a5a3981ca34aa8e0e105f0fb8be6a496b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_57224cd0c199ce4235aa94c0d7cfe69a5a3981ca34aa8e0e105f0fb8be6a496b->leave($__internal_57224cd0c199ce4235aa94c0d7cfe69a5a3981ca34aa8e0e105f0fb8be6a496b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
    }
}
