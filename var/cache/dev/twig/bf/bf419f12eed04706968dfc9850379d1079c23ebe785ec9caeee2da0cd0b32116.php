<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_52e2de02614a51833cfe519cc2e6d6fd61af3f81787dfbe4da813677075d70a5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_81b40758f72964e3c97c744336a14fd4abcec0a36aee31f380093e3b82755e1a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_81b40758f72964e3c97c744336a14fd4abcec0a36aee31f380093e3b82755e1a->enter($__internal_81b40758f72964e3c97c744336a14fd4abcec0a36aee31f380093e3b82755e1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_81b40758f72964e3c97c744336a14fd4abcec0a36aee31f380093e3b82755e1a->leave($__internal_81b40758f72964e3c97c744336a14fd4abcec0a36aee31f380093e3b82755e1a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
    }
}
