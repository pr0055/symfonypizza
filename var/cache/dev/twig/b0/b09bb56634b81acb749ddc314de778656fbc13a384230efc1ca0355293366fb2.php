<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_d6db34d7bea95110bd05c1dcfd4764a9a3fdc26a10f51a0d3bcd2388fdbbf9ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e54574d5e10f1034a76683bcdefa5920067453a116618de0d3b44be700cd8cf0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e54574d5e10f1034a76683bcdefa5920067453a116618de0d3b44be700cd8cf0->enter($__internal_e54574d5e10f1034a76683bcdefa5920067453a116618de0d3b44be700cd8cf0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_e54574d5e10f1034a76683bcdefa5920067453a116618de0d3b44be700cd8cf0->leave($__internal_e54574d5e10f1034a76683bcdefa5920067453a116618de0d3b44be700cd8cf0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
    }
}
