<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_a158dc4333b7920c077d8b05540443c17c10610eb8716f40f96e140f5a008ec5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6d284ccf473decd326dc4b724e50c41d458c57de377f84f91e82a90d5298084c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d284ccf473decd326dc4b724e50c41d458c57de377f84f91e82a90d5298084c->enter($__internal_6d284ccf473decd326dc4b724e50c41d458c57de377f84f91e82a90d5298084c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_6d284ccf473decd326dc4b724e50c41d458c57de377f84f91e82a90d5298084c->leave($__internal_6d284ccf473decd326dc4b724e50c41d458c57de377f84f91e82a90d5298084c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
    }
}
