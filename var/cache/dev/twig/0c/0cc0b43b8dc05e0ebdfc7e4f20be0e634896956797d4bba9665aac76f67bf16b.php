<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_ef71ac06f66dc577042751f3f001526ab4752f70470bfba2eca3d1e59dc4ff6a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9293ee8d2c2a3c8d04f37cd9cff362d62f0ab33157b3ee675e04a5245652bfd1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9293ee8d2c2a3c8d04f37cd9cff362d62f0ab33157b3ee675e04a5245652bfd1->enter($__internal_9293ee8d2c2a3c8d04f37cd9cff362d62f0ab33157b3ee675e04a5245652bfd1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_9293ee8d2c2a3c8d04f37cd9cff362d62f0ab33157b3ee675e04a5245652bfd1->leave($__internal_9293ee8d2c2a3c8d04f37cd9cff362d62f0ab33157b3ee675e04a5245652bfd1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
    }
}
