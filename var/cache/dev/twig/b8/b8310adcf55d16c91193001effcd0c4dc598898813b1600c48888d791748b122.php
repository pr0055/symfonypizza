<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_32a0bfe6bfebe1e4486ebb0ab46b4eb63ff1aaf5318be668c85260ba416b20d9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f75589692fe15a13dcebb5dc3270c5dd308e39ed77d20e94c3f89d0fcee9bc7b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f75589692fe15a13dcebb5dc3270c5dd308e39ed77d20e94c3f89d0fcee9bc7b->enter($__internal_f75589692fe15a13dcebb5dc3270c5dd308e39ed77d20e94c3f89d0fcee9bc7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_f75589692fe15a13dcebb5dc3270c5dd308e39ed77d20e94c3f89d0fcee9bc7b->leave($__internal_f75589692fe15a13dcebb5dc3270c5dd308e39ed77d20e94c3f89d0fcee9bc7b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
    }
}
