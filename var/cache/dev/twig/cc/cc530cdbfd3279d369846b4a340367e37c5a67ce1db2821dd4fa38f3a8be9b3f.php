<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_1007bab0f59cff3e64249e7ed7b6805693d2a2fffc0507d72951ad491c72a886 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3d690bbdcc70baf125ce27cd35d89676b983f000c906b270aa7e929389f08d0c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3d690bbdcc70baf125ce27cd35d89676b983f000c906b270aa7e929389f08d0c->enter($__internal_3d690bbdcc70baf125ce27cd35d89676b983f000c906b270aa7e929389f08d0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_3d690bbdcc70baf125ce27cd35d89676b983f000c906b270aa7e929389f08d0c->leave($__internal_3d690bbdcc70baf125ce27cd35d89676b983f000c906b270aa7e929389f08d0c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
    }
}
