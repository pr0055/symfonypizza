<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_0dd35ebb9c9e9b34f4dd830a25cd4506136e85f29d10a21e3727cdf58bec9279 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b27a03cbc4769e49d0993c030179f5ed66aff974bcd588ab1ffb7a12dcfca7cc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b27a03cbc4769e49d0993c030179f5ed66aff974bcd588ab1ffb7a12dcfca7cc->enter($__internal_b27a03cbc4769e49d0993c030179f5ed66aff974bcd588ab1ffb7a12dcfca7cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.js.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_b27a03cbc4769e49d0993c030179f5ed66aff974bcd588ab1ffb7a12dcfca7cc->leave($__internal_b27a03cbc4769e49d0993c030179f5ed66aff974bcd588ab1ffb7a12dcfca7cc_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }

    public function getSource()
    {
        return "/*
{% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}
*/
";
    }
}
