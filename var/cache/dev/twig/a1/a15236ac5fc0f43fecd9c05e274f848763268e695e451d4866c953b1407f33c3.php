<?php

/* base.html.twig */
class __TwigTemplate_8632e9dbe2cebd816cf74a9b200d2cfe5e42da6d95e4cc900dc4ce7485fcdbb5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8a9a210185bfe76f2b6ccc1baead44b3e68f1d762b0a97380fdadc2eaa772134 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a9a210185bfe76f2b6ccc1baead44b3e68f1d762b0a97380fdadc2eaa772134->enter($__internal_8a9a210185bfe76f2b6ccc1baead44b3e68f1d762b0a97380fdadc2eaa772134_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_8a9a210185bfe76f2b6ccc1baead44b3e68f1d762b0a97380fdadc2eaa772134->leave($__internal_8a9a210185bfe76f2b6ccc1baead44b3e68f1d762b0a97380fdadc2eaa772134_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_70deb7465cbe1e0f155739cfd344eadf3b3e108fa1b73b6d77594ac3599bdff9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_70deb7465cbe1e0f155739cfd344eadf3b3e108fa1b73b6d77594ac3599bdff9->enter($__internal_70deb7465cbe1e0f155739cfd344eadf3b3e108fa1b73b6d77594ac3599bdff9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_70deb7465cbe1e0f155739cfd344eadf3b3e108fa1b73b6d77594ac3599bdff9->leave($__internal_70deb7465cbe1e0f155739cfd344eadf3b3e108fa1b73b6d77594ac3599bdff9_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_03363b9f3d6e4e6563c1bdf5497e9b450a367f45e289b8dda04093d78d0c0179 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_03363b9f3d6e4e6563c1bdf5497e9b450a367f45e289b8dda04093d78d0c0179->enter($__internal_03363b9f3d6e4e6563c1bdf5497e9b450a367f45e289b8dda04093d78d0c0179_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_03363b9f3d6e4e6563c1bdf5497e9b450a367f45e289b8dda04093d78d0c0179->leave($__internal_03363b9f3d6e4e6563c1bdf5497e9b450a367f45e289b8dda04093d78d0c0179_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_317b5a4903b13969e8e689e210c7ffb12f29562da2e3a2dbc6822e3daa6955b2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_317b5a4903b13969e8e689e210c7ffb12f29562da2e3a2dbc6822e3daa6955b2->enter($__internal_317b5a4903b13969e8e689e210c7ffb12f29562da2e3a2dbc6822e3daa6955b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_317b5a4903b13969e8e689e210c7ffb12f29562da2e3a2dbc6822e3daa6955b2->leave($__internal_317b5a4903b13969e8e689e210c7ffb12f29562da2e3a2dbc6822e3daa6955b2_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_45e9610d442211013ebfeb2cffb448e7d7e81cc6ad1805e41943fad26c0cf056 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_45e9610d442211013ebfeb2cffb448e7d7e81cc6ad1805e41943fad26c0cf056->enter($__internal_45e9610d442211013ebfeb2cffb448e7d7e81cc6ad1805e41943fad26c0cf056_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_45e9610d442211013ebfeb2cffb448e7d7e81cc6ad1805e41943fad26c0cf056->leave($__internal_45e9610d442211013ebfeb2cffb448e7d7e81cc6ad1805e41943fad26c0cf056_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }

    public function getSource()
    {
        return "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
";
    }
}
