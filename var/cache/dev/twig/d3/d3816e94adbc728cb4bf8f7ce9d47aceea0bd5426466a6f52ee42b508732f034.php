<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_53276a9f415677767a13f9dd16b2ec22e3085c8ada45f5903ecab077daa8dafb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_95c61a0fed27691b6beac7a4b83dae2e0a218fa0a4a5e6ee60317252caf58786 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_95c61a0fed27691b6beac7a4b83dae2e0a218fa0a4a5e6ee60317252caf58786->enter($__internal_95c61a0fed27691b6beac7a4b83dae2e0a218fa0a4a5e6ee60317252caf58786_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_95c61a0fed27691b6beac7a4b83dae2e0a218fa0a4a5e6ee60317252caf58786->leave($__internal_95c61a0fed27691b6beac7a4b83dae2e0a218fa0a4a5e6ee60317252caf58786_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
    }
}
