<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_0b75050742ba4151d687631628bb0d28cb85bf6f7fd4d74d6b5a2a09422c4566 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_04d2da21c06e3da33e2217923f541ae77cc3505d32f23e7e78134a770be0e3fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_04d2da21c06e3da33e2217923f541ae77cc3505d32f23e7e78134a770be0e3fd->enter($__internal_04d2da21c06e3da33e2217923f541ae77cc3505d32f23e7e78134a770be0e3fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_04d2da21c06e3da33e2217923f541ae77cc3505d32f23e7e78134a770be0e3fd->leave($__internal_04d2da21c06e3da33e2217923f541ae77cc3505d32f23e7e78134a770be0e3fd_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
    }
}
