<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_630140ef05c2f1e2dfa94bcbc57202b6b6029e8343cefb0eb6ec1d8f1d9ca35b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3f54b3da91651176e10635da22b6d559a1a8a75269b445ab3f057afd21cc129d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3f54b3da91651176e10635da22b6d559a1a8a75269b445ab3f057afd21cc129d->enter($__internal_3f54b3da91651176e10635da22b6d559a1a8a75269b445ab3f057afd21cc129d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_3f54b3da91651176e10635da22b6d559a1a8a75269b445ab3f057afd21cc129d->leave($__internal_3f54b3da91651176e10635da22b6d559a1a8a75269b445ab3f057afd21cc129d_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "{{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}
";
    }
}
