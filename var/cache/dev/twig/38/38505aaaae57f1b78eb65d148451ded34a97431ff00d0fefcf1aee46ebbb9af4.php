<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_b72acc5e98cbc91c6ceea0e9e13345957d9fa9091c14b5b1acbd8d75803791e5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8dc4549559a4896bb12a9ed1f4f9a3b90a4eaeee5725030b8b053332ff08fe33 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8dc4549559a4896bb12a9ed1f4f9a3b90a4eaeee5725030b8b053332ff08fe33->enter($__internal_8dc4549559a4896bb12a9ed1f4f9a3b90a4eaeee5725030b8b053332ff08fe33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_8dc4549559a4896bb12a9ed1f4f9a3b90a4eaeee5725030b8b053332ff08fe33->leave($__internal_8dc4549559a4896bb12a9ed1f4f9a3b90a4eaeee5725030b8b053332ff08fe33_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'password')) ?>
";
    }
}
