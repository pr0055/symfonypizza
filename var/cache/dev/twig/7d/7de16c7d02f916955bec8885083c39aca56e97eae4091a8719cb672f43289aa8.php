<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_9671e7eade6a7dd10aa0f61d3f638d0dcae1ad714351b66d0157680ead7f3ecf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bdff4d1ad0f201f580fa297e5b66b94fd58aea2aa4ae85bfe3dac9a7084aa557 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bdff4d1ad0f201f580fa297e5b66b94fd58aea2aa4ae85bfe3dac9a7084aa557->enter($__internal_bdff4d1ad0f201f580fa297e5b66b94fd58aea2aa4ae85bfe3dac9a7084aa557_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_bdff4d1ad0f201f580fa297e5b66b94fd58aea2aa4ae85bfe3dac9a7084aa557->leave($__internal_bdff4d1ad0f201f580fa297e5b66b94fd58aea2aa4ae85bfe3dac9a7084aa557_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    public function getSource()
    {
        return "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
    }
}
