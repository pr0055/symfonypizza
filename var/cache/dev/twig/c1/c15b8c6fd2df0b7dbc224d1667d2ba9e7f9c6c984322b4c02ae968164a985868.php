<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_644aea261fd5ff352f18a510b135f0e465c1228ade9e081bb140efef8b57cad0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e97369260f42d9e512d80c46df51faf01ce800c35ef0ba03db2fddc42f94b90d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e97369260f42d9e512d80c46df51faf01ce800c35ef0ba03db2fddc42f94b90d->enter($__internal_e97369260f42d9e512d80c46df51faf01ce800c35ef0ba03db2fddc42f94b90d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e97369260f42d9e512d80c46df51faf01ce800c35ef0ba03db2fddc42f94b90d->leave($__internal_e97369260f42d9e512d80c46df51faf01ce800c35ef0ba03db2fddc42f94b90d_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_6583c4ca878d8ca8fa43b7cc9965fd8ef53bbd101df76e2a42d2d987761701ad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6583c4ca878d8ca8fa43b7cc9965fd8ef53bbd101df76e2a42d2d987761701ad->enter($__internal_6583c4ca878d8ca8fa43b7cc9965fd8ef53bbd101df76e2a42d2d987761701ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_6583c4ca878d8ca8fa43b7cc9965fd8ef53bbd101df76e2a42d2d987761701ad->leave($__internal_6583c4ca878d8ca8fa43b7cc9965fd8ef53bbd101df76e2a42d2d987761701ad_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_f59c43f186568483d73b0cac3ece97078ff5be4b7e8dd801ccf742d3e1584fd9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f59c43f186568483d73b0cac3ece97078ff5be4b7e8dd801ccf742d3e1584fd9->enter($__internal_f59c43f186568483d73b0cac3ece97078ff5be4b7e8dd801ccf742d3e1584fd9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_f59c43f186568483d73b0cac3ece97078ff5be4b7e8dd801ccf742d3e1584fd9->leave($__internal_f59c43f186568483d73b0cac3ece97078ff5be4b7e8dd801ccf742d3e1584fd9_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_33801d7c565e5a6d59b2cd520ecf72fc32496fbc6f9cd07f6387d4d256cd1bfe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_33801d7c565e5a6d59b2cd520ecf72fc32496fbc6f9cd07f6387d4d256cd1bfe->enter($__internal_33801d7c565e5a6d59b2cd520ecf72fc32496fbc6f9cd07f6387d4d256cd1bfe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_33801d7c565e5a6d59b2cd520ecf72fc32496fbc6f9cd07f6387d4d256cd1bfe->leave($__internal_33801d7c565e5a6d59b2cd520ecf72fc32496fbc6f9cd07f6387d4d256cd1bfe_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
";
    }
}
