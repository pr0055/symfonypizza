<?php

/* TwigBundle:Exception:error.txt.twig */
class __TwigTemplate_20bd6efc92ffb79ab99cfb9f780d983b1e5a47101e1eb9ab9f15879f7c864115 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_edd87199a2475db16766f207b1a87b9fc8f08568f1a64fb00d36c20d3e695310 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_edd87199a2475db16766f207b1a87b9fc8f08568f1a64fb00d36c20d3e695310->enter($__internal_edd87199a2475db16766f207b1a87b9fc8f08568f1a64fb00d36c20d3e695310_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code"));
        echo " ";
        echo (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_edd87199a2475db16766f207b1a87b9fc8f08568f1a64fb00d36c20d3e695310->leave($__internal_edd87199a2475db16766f207b1a87b9fc8f08568f1a64fb00d36c20d3e695310_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 4,  22 => 1,);
    }

    public function getSource()
    {
        return "Oops! An Error Occurred
=======================

The server returned a \"{{ status_code }} {{ status_text }}\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
    }
}
