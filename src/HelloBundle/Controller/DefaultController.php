<?php

namespace HelloBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\HttpFoundation\Request;
use HelloBundle\Entity\Orders;
use HelloBundle\Entity\Pizza;
use HelloBundle\Entity\Slices;
use HelloBundle\Entity\Pizzeria;
use HelloBundle\Entity\ToppingIngredient;
use HelloBundle\Entity\Ingredient;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
class DefaultController extends Controller
{
    public function deleteAction($id){
        $em=$this->getDoctrine()->getManager();
        $deleteorder=$em->getRepository('HelloBundle:Orders')->find($id);
        $em->remove($deleteorder);
        $em->flush();
        // do zrobionia powiazanie pomiedzy encja pizza i encja slice
        /*$pizza = new Pizza();
        $deleteslice=$em->getRepository('HelloBundle:Slices')->findOneBypizzaid($pizza);
        if($deleteslice){
            $em->remove($deleteslice);
            $em->flush();
        }
        $deletepizza=$em->getRepository('HelloBundle:Pizza')->findOneByorderid($id);
        if($deletepizza) {
            $em->remove($deletepizza);
            $em->flush();
        }*/
        // deleting slices from database
        $em=$this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'DELETE FROM HelloBundle:Slices s WHERE s.pizzaid IN (SELECT p.id FROM HelloBundle:Pizza p WHERE p.orderid=?1)')
            ->setParameter('1', $id);
        $query->getResult();

        // deleting pizzas from database
        $em=$this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'DELETE FROM HelloBundle:Pizza p WHERE p.orderid=?1')
            ->setParameter('1', $id);
        $query->getResult();

        $response = $this->forward('HelloBundle:Default:index');
        return $response;
    }
    public function editAction($id, Request $request2)
    {
        $session = $request2->getSession();
        $session->set('id', $id);
        $em=$this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT o.id FROM HelloBundle:Orders o WHERE o.id=?1')
            ->setParameter('1', $id);
        $check = $query->getResult();

        if($check==TRUE){
        $em=$this->getDoctrine()->getManager();
        $query = $em->createQuery(
           'SELECT s.id, s.toppingid, s.pizzaid FROM HelloBundle:Slices s WHERE s.pizzaid IN (SELECT p.id FROM HelloBundle:Pizza p WHERE p.orderid=?1)')
        ->setParameter('1', $id);
        $edittopping = $query->getResult();
        //$pizz=-1;

        $pizza = new Pizza();
        $form = $this->createFormBuilder($pizza)
            ->add('slicecount', IntegerType::class)
            ->add('toppingcount', IntegerType::class)
            ->add('Add pizza', SubmitType::class)
            ->getForm();

        $form->handleRequest($request2);
        if ($form->isSubmitted() && $form->isValid()) {

            $slicecount=$form['slicecount']->getData();
            $toppingcount=$form['toppingcount']->getData();
            $countoftopping=$toppingcount;
            $pizza->setOrderid($id);
            $pizza->setSlicecount($slicecount);
            $pizza->setToppingcount($toppingcount);
            $em = $this->getDoctrine()->getManager();
            $em->persist($pizza);
            $em->flush();
            $lastpizzaid=$pizza->getId();
            $pizzaid=$lastpizzaid;
            for($i=0;$i<$toppingcount;$i++) {
                $slice = new Slices();
                $newpizzaid=$lastpizzaid;
                $pizz=$lastpizzaid;
                $slice->setPizzaid($newpizzaid);
                $slice->setToppingid('1');
                $em = $this->getDoctrine()->getManager();
                $em->persist($slice);
                $em->flush();
                dump($slice);
                $em=$this->getDoctrine()->getManager();
                $query = $em->createQuery(
                    'SELECT s.id, s.toppingid, s.pizzaid FROM HelloBundle:Slices s WHERE s.pizzaid IN (SELECT p.id FROM HelloBundle:Pizza p WHERE p.orderid=?1)')
                    ->setParameter('1', $id);
                $edittopping = $query->getResult();
               // $pizza->addSlice($slice);
                // 'SELECT pizzaId, s.id AS sliceId, t.name AS topping, t.cost, p.name AS pizzeria, phoneNumber
               // FROM slice s JOIN topping t ON s.toppingId=t.id JOIN pizzeria p ON t.pizzeriaId=p.id
           // JOIN pizza ON pizza.id = pizzaId WHERE orderId = ?;');
            }

        }
        $toppingall=$this->getDoctrine()->getRepository('HelloBundle:Topping')->findAll();
        //$edittopping=$this->getDoctrine()->getRepository('HelloBundle:Slices')->findBypizzaid($pizz);
        //$edittopping=$this->getDoctrine()->getRepository('HelloBundle:Slices')->findAll();
        $edittopping2=$this->getDoctrine()->getRepository('HelloBundle:Pizza')->findAll();
      //  var_dump($pizzaid);

        return $this->render('HelloBundle:Default:edit.html.twig', array('id' => $id, 'form' => $form->createView(),'edittopping' => $edittopping, 'toppingall'=>$toppingall, 'edittopping2' => $edittopping2));
        }
        else{
            return new Response('Bledne id zamowienia');
        }
    }



    public function indexAction( Request $request)
    {
        $orders = new Orders();
        $form = $this->createFormBuilder($orders)
            ->add('name', TextType::class)
            ->add('date', DateTimeType::class)
            ->add('Add new order', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $name=$form['name']->getData();
            $date=$form['date']->getData();

            $orders->setName($name);
            $orders->setDate($date);

            $em = $this->getDoctrine()->getManager();
            $em->persist($orders);
            $em->flush();
        }
        $neworders=$this->getDoctrine()->getRepository('HelloBundle:Orders')->findAll();


        return $this->render('HelloBundle:Default:index.html.twig', array('form' => $form->createView(), 'neworders'=>$neworders));
    }

    public function editsliceAction($id, Request $request, Request $request2)
    {
        $session = $request->getSession();
        $foo = $session->get('id');
        $em=$this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT s.id FROM HelloBundle:Slices s WHERE s.id=?1')
            ->setParameter('1', $id);
        $check = $query->getResult();
        if($check==TRUE) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery(
                'SELECT t.id, t.name, t.cost, p.name AS pizzeria
            FROM HelloBundle:Topping t JOIN HelloBundle:Pizzeria p WITH p.id=t.pizzeriaid ORDER BY t.id');
            $toppingparametr = $query->getResult();
            $em2 = $this->getDoctrine()->getManager();
            $query2 = $em->createQuery(
                'SELECT t.id, t.toppingId, t.ingredientId, i.name
            FROM HelloBundle:ToppingIngredient t JOIN HelloBundle:Ingredient i WITH t.ingredientId = i.id ORDER BY t.id');
            $ingredientsparameter = $query2->getResult();

            $slice = new Slices();
            $form = $this->createFormBuilder($slice)
                ->add('Change topping', SubmitType::class)
                ->getForm();
            $form->handleRequest($request);

            return $this->render('HelloBundle:Default:editslice.html.twig', array('form' => $form->createView(), 'toppingparametr' => $toppingparametr, 'ingredientsparameter' => $ingredientsparameter, 'id' => $id, 'foo' => $foo));
        }
        else{
            return new Response('Bledne id kawalka');
        }
    }

    public function updatesliceAction(Request $request, $toppid, $ident){
        $session = $request->getSession();
        $orderid = $session->get('id');
        $sliceid = $ident;
        $em = $this->getDoctrine()->getManager();
        $update = $em->getRepository('HelloBundle:Slices')->find($sliceid);
        var_dump($toppid);
        var_dump($sliceid);
        var_dump($orderid);
        $update->setToppingid($toppid);
        $em->flush();

       // $response = $this->forward('HelloBundle:Default:edit', array('id'=>$orderid));
        //return $response;
        return $this->redirect($this->generateUrl('editorder', array('id'=>$orderid)));
        //return $this->render('HelloBundle:Default:edit.html.twig', array('id'=>$sliceid));
    }
}
